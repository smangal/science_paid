package com.mathfriendzy.serveroperation;;

/**
 * This is the response interface , Each Activity implements this where we want to do the server operation
 * @author Yashwant Singh
 *
 */
public interface HttpResponseInterface {
	
	/**
	 * This method gives the response from the server  , 
	 * and also gives the request code for which we want to get response from server
	 * @param httpResponseBase
	 * @param requestCode
	 */
	void serverResponse(HttpResponseBase httpResponseBase, int requestCode);
}
