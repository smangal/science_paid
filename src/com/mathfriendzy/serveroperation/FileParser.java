package com.mathfriendzy.serveroperation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.controller.resources.GetKhanVideoLinkResponse;
import com.mathfriendzy.controller.resources.ResourceCategory;
import com.mathfriendzy.controller.resources.ResourceResponse;
import com.mathfriendzy.controller.resources.ResourceSubCat;
import com.mathfriendzy.controller.resources.SearchResourceResponse;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusResponse;
import com.mathfriendzy.newinappclasses.GetResourceVideoInAppResult;
import com.mathfriendzy.utils.CommonUtils;


/**
 * This is the File Parser class 
 * Which Parse either json or XMl
 * @author Yashwant Singh
 *
 */
public class FileParser {

	private static final String TAG = "FileParser";

	//for download pdf at the time of homework download
	//private ArrayList<String> pdfNameList = null;
	public HttpResponseBase ParseJsonString(String response, int requestCode) {
		if(requestCode == ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
			return this.parseResourceVideoInAppStatusResponse(response);
		else if(requestCode == ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
			return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
		else if(requestCode == ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST)
			return this.parseGetAppUnlockStatusResponse(response);
		else if(requestCode == ServerOperationUtil.UPDATE_TEACHER_CREDIT_FOR_STUDENT_ANSWER)
			return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
		else if(requestCode == ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER)
			return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
		else if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST)
			return this.parseResourceCategories(response);
		else if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST)
			return this.parseSearchResultResponse(response);
		else if(requestCode == ServerOperationUtil.GET_KHAN_VIDEO_LINK)
            return this.paserKhanVideoLinkResponse(response);
		return null;
	}

	/**
	 * Parse the Resource video inApp Status response
	 * @param jsonString
	 * @return
	 */
	private GetResourceVideoInAppResult parseResourceVideoInAppStatusResponse(String jsonString) {
		try {
			CommonUtils.printLog(TAG, "inside parseResourceVideoInAppStatusResponse response " + jsonString);
			GetResourceVideoInAppResult response = new GetResourceVideoInAppResult();
			JSONObject jsonObj = new JSONObject(jsonString);
			response.setResult(jsonObj.getString("result"));
			response.setStatus(jsonObj.getInt("videoPurchase"));
			response.setUnlockCategoryStatus(jsonObj.getInt("unlockCategories"));
			return response;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Parse the response which doen not have nothing to parse
	 * @param jsonString
	 * @return
	 */
	private HttpResponseBase parseTheResponseWhichDoesNotHaveNothingToShow(String jsonString) {
		HttpResponseBase response = new HttpResponseBase();
		if(CommonUtils.LOG_ON)
			Log.e(TAG , "inside parseTheResponseWhichDoesNotHaveNothingToShow response " + jsonString);
		return response;
	}

	/**
	 * Parse the AppUnlock status from server
	 * @param jsonString
	 * @return
	 */
	private GetAppUnlockStatusResponse parseGetAppUnlockStatusResponse(String jsonString) {
		try {
			CommonUtils.printLog(TAG, "inside parseResourceVideoInAppStatusResponse response " + jsonString);
			GetAppUnlockStatusResponse response = new GetAppUnlockStatusResponse();
			JSONObject jsonObj = new JSONObject(jsonString);
			response.setResult(jsonObj.getString("result"));
			if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj ,
					"appsUnlock")) {
				response.setAppUnlockStatus(jsonObj.getInt("appsUnlock"));
			}
			response.setCoins(jsonObj.getInt("coins"));
			return response;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public ResourceCategory parseResourceCategories(String jsonString) {
		try {
			CommonUtils.printLog(TAG , "inside parseResourceCategories response " + jsonString);
			ResourceCategory resourceCategory = new ResourceCategory();
			ArrayList<ResourceCategory> resourceCategoryList = new ArrayList<ResourceCategory>();
			JSONObject jsonObject = new JSONObject(jsonString);
			resourceCategory.setResult(jsonObject.getString("result"));

			JSONArray jsonCatArray = jsonObject.getJSONArray("data");
			for(int i = 0 ; i < jsonCatArray.length() ; i ++ ){
				JSONObject jsonCatObj = jsonCatArray.getJSONObject(i);
				ResourceCategory cat = new ResourceCategory();
				cat.setCatId(jsonCatObj.getInt("id"));
				cat.setCatName(MathFriendzyHelper
						.replaceDoubleQuotesBySingleQuote(jsonCatObj.getString("title")));

				JSONArray jsonSubCatArray = jsonCatObj.getJSONArray("subcategories");
				ArrayList<ResourceSubCat> subCatList = new ArrayList<ResourceSubCat>();
				for(int j = 0 ; j < jsonSubCatArray.length() ; j ++ ){
					JSONObject jsonSubCatObj = jsonSubCatArray.getJSONObject(j);
					ResourceSubCat subCat = new ResourceSubCat();
					subCat.setSubCatId(jsonSubCatObj.getInt("subCategId"));
					subCat.setSubCatName(MathFriendzyHelper
							.replaceDoubleQuotesBySingleQuote(jsonSubCatObj.getString("title")));
					subCatList.add(subCat);
				}
				cat.setSubCatList(subCatList);
				resourceCategoryList.add(cat);
			}
			resourceCategory.setCategoriesList(resourceCategoryList);
			resourceCategory.setDate(MathFriendzyHelper
					.getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
			if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "subjects")){
				resourceCategory.setSubjectList(
						MathFriendzyHelper
						.getCommaSepratedOption(jsonObject.getString("subjects"),","));
			}
			return resourceCategory;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	private SearchResourceResponse parseSearchResultResponse(String jsonString) {
		SearchResourceResponse resources = new SearchResourceResponse();
		JSONObject data = null;
		JSONArray results = null;
		try{
			CommonUtils.printLog(TAG , "inside parseSearchResultResponse response " + jsonString);
			data =  new JSONObject(jsonString);
			if(data.has("results") && data.getJSONArray("results").length() > 0){
				results = data.getJSONArray("results");
				resources.setTotalHitCount(data.getJSONObject("stats")
						.get("totalHitCount").toString());
				final int lenth = results.length();
				for(int i = 0; i < lenth; i++){
					ResourceResponse resource = new ResourceResponse();
					JSONObject result = results.getJSONObject(i);
					resource.setTitle(result.get("title").toString());
					resource.setMediaType(result.get("mediaType").toString());
					resource.setDescription(result.get("description").toString());
					resource.setValue(result.getJSONObject("resourceFormat").get("value").toString());
					resource.setUrl(result.getJSONObject("thumbnails").get("url").toString());
					resource.setResourceUrl(result.getJSONObject("thumbnails")
							.get("resourceUrl").toString());
					resource.setScollectionCount(result.get("scollectionCount").toString());
					JSONArray curriculumCodeArray = result.getJSONArray("curriculumCode");
					final int curriculumCodeLength = curriculumCodeArray.length();
					for(int j = 0; j < curriculumCodeLength; j++)
						resource.getCurriculumCode().add(curriculumCodeArray.getString(j));
					resources.getListOfresource().add(resource);
				}
				resources.setResult(MathFriendzyHelper.SUCCESS);
			}else{
				resources.setResult("Fail");
			}
		}catch(Exception e){
			e.printStackTrace();
			resources.setResult("Fail");
		}
		return resources;
	}

	/**
     * Parse the vedio link from khan academy url
     * @param jsonString
     * @return
     */
    private GetKhanVideoLinkResponse paserKhanVideoLinkResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside paserKhanVideoLinkResponse response " + jsonString);
            GetKhanVideoLinkResponse response = new GetKhanVideoLinkResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setLink(jsonObj.getString("link"));
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
