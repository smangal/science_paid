package com.mathfriendzy.utils;

import java.util.ArrayList;

import com.mathfriendzy.model.friendzy.FriendzyDTO;

public class FriendzyUtils {

	//changes for friendzy challenge
	 public static boolean isActivePlayer = false;
	 public static ArrayList<FriendzyDTO> activeFriendzyPlayerList = new ArrayList<FriendzyDTO>();
	 //end for friendzy
	
	/**
	 * This method check for active player for friendzy challenge
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public static boolean isActivePlayer(String userId , String playerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)){
				if(DateTimeOperation.getDuration(activeFriendzyPlayerList.get(i).getEndDate()) >= 0){
					isActivePlayer = true;
					return true ;
				}else{
					isActivePlayer = false;
					activeFriendzyPlayerList.remove(i);
					return false;
				}
			}
		}
		isActivePlayer = false;
		return false;
	}

	/**
	 * This method return the active player challengerId
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public static String getActivePlayerChallengerId(String userId , String playerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)){
				return activeFriendzyPlayerList.get(i).getChallengerId();
			}
		}
		return "";
	}

	/**
	 * This method check for player active or not friendzy challenge
	 * @param challengerId
	 * @param playerId
	 * @return
	 */
	public static boolean isChallengerActiveOrNot(String challengerId , String playerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
					&& activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
				return true;
		}
		return false;
	}

	/**
	 * This method update the new end date for active player
	 * @param playerId
	 * @param endData
	 */
	public static void updateEndDateForFriendzyChallenge(String playerId , String endDate , String challengerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
					&& activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
			{
				activeFriendzyPlayerList.get(i).setEndDate(endDate);
			}
		}
	}

	
	/**
	 * This method deactivate the challenger
	 * @param challengerId
	 * @param playerId
	 */
	public static void deActivatePlayer(String challengerId , String playerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
					&& activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
				activeFriendzyPlayerList.remove(i);
		}
	}

	/**
	 * This method remove the challenge from the list 
	 * when user accept the another challenge
	 * @param playerId
	 */
	public static void remeoveChallenge(String playerId){
		for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
			if(activeFriendzyPlayerList.get(i).getPid().equals(playerId))
				activeFriendzyPlayerList.remove(i);
		}
	}

}
